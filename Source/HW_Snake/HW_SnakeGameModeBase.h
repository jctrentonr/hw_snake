// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HW_SnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HW_SNAKE_API AHW_SnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
