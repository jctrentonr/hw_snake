// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	LastMoveDirection = EMovementDirection::UP;
	ElementSize = 60;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	SnakeElements[SnakeElements.Num() - 1]->MeshComponent->SetVisibility(true);
}

void ASnakeBase::AddSnakeElement(int ElementsNum, bool onFood)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		// FIXME need fix. Snake should be able to spawn anywhere, not only in the (0,0,0)
		ASnakeElementBase* SnakeElem;
		if (SnakeElements.Num() == 0)
		{
			auto NewTransform = FTransform(FVector(0,0,0));
			SnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		}
		else {
			auto NewLocation = FVector(SnakeElements.Num() * 60, 0, 0);
			auto NewTransform = FTransform(FVector(0, 0, 0) - NewLocation);
			SnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		}
		
		//if (onFood)
		//{
		//	SnakeElem->MeshComponent->SetVisibility(false);
		//}
		SnakeElem->SnakeOwner = this;

		int32 ElemIndex = SnakeElements.Add(SnakeElem);
		

		if (ElemIndex == 0)
		{
			SnakeElem->SetFirstElementType();
		}
	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP :
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN :
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT :
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT :
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	// ��������� ������������ ��� ������, ����� �� "���������" ��� ������ �� ��������
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurElem = SnakeElements[i];
		auto PrevElem = SnakeElements[i - 1];
		FVector PrevLocation = PrevElem->GetActorLocation();
		CurElem->SetActorLocation(PrevLocation);
	}

	// Head is moving along from other elements
	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	// �������� ������������ �������
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;

		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::IncreaseSpeed()
{
	float NewTickInterval = GetActorTickInterval() - SpeedChange;
	SetActorTickInterval(fmaxf(NewTickInterval, MaxSpeed));
}

